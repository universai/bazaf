create or replace function recalc_spc()
returns trigger as $$
begin 
	if tg_name = 'inserted_spc' then
		update supplies as s
		set "cost" = "cost" + new.volume * (p.b_price + p.markup)
		from products as p
		where s.id = new.supply_id and p.id = new.product_id;
	end if;
	if tg_name = 'updated_spc' then
		--unplug old
		update supplies as s
		set "cost" = "cost" - old.volume * (p.b_price + p.markup)
		from products as p
		where s.id = old.supply_id and p.id = old.product_id;
		--plug new
		update supplies as s
		set "cost" = "cost" + new.volume * (p.b_price + p.markup)
		from products as p
		where s.id = new.supply_id and p.id = new.product_id;
	end if;
	if tg_name = 'deleted_spc' then
		update supplies as s
		set "cost" = "cost" - old.volume * (p.b_price + p.markup)
		from products as p
		where s.id = old.supply_id and p.id = old.product_id;
		delete from supplies as s
		where s.id = old.supply_id and s.cost = 0;
	end if;
	if tg_name = 'updated_product' then
		update supplies as s
		set "cost" = "cost" + spc.volume * (new.b_price - old.b_price + new.markup - old.markup)
		from sup_prod_cons as spc
		where s.id = spc.supply_id and new.id = spc.product_id;
	end if;
	return new;
end;
$$ language 'plpgsql';


create trigger inserted_spc
	after insert on sup_prod_cons
	for each row
	execute function recalc_spc();

create trigger updated_spc
	after update on sup_prod_cons
	for each row
	execute function recalc_spc();
	
create trigger deleted_spc
	after delete on sup_prod_cons
	for each row 
	execute function recalc_spc();
	
create trigger updated_product
	after update on products
	for each row
	execute function recalc_spc();