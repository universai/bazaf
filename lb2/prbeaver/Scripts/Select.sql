--Самые выгодные поставки
select s.*, s."cost" - sum(spc.volume * p.b_price) as surplus
from supplies s 
join sup_prod_cons spc on s.id = spc.supply_id 
join products p on spc.product_id = p.id
group by s.id 
order by surplus desc;

--Объемы проданной продукции
select p.id, p."name", sum(spc.volume) as overall_volume, unit_name 
from  products p
join sup_prod_cons spc on p.id = spc.product_id 
group by p.id
order by overall_volume desc;

--Самые уважаемые партнеры
select c.id, c."name", sum(s."cost") as overall_cost, count(s."cost") as overall_supplies
from companies c 
join supplies s on c.id = s.company_id 
group by c.id 
order by overall_cost desc;

--Установить адекватную наценку
update products as p
set markup = markup - b_price 
where markup > b_price;

--Увеличить объемы поставок в 3 самые отстающие компании
with unresp as (
	select c.id as id, c."name", sum(s."cost") as overall_cost, count(s."cost") as overall_supplies
	from companies c 
	join supplies s on c.id = s.company_id 
	group by c.id 
	order by overall_cost asc
	limit 3
)
update sup_prod_cons as spc
set volume = volume * 2
from supplies s, unresp u
where spc.supply_id = s.id and s.company_id = u.id

--Удалить все поставки с 1 продуктом (CASCADE)
with singl as (
	select s.id as id, count(spc.product_id) as prod_num
	from supplies s 
	join sup_prod_cons spc on s.id = spc.supply_id
	group by s.id 
	having count(spc.product_id) = 1
)
delete from supplies s
using singl
where s.id = singl.id

--Установить правильные значения стоимости у поставок
with scos as (
	select s.id as id, sum(spc.volume * (p.b_price + p.markup)) as cost
	from supplies s 
	join sup_prod_cons spc ON s.id = spc.supply_id 
	join products p on spc.product_id = p.id
	group by s.id)
update supplies as s
set cost=sc.cost
from scos as sc
where s.id = sc.id