create table supplies (
	id integer primary key,
	"cost" decimal not null, 
	"date" date not null,
	company_id integer not null,
	--constraints
	constraint non_neg_cost check("cost" >= 0)
);

create table products (
	id integer primary key,
	"name" varchar(100) not null,
	b_price decimal not null,
	markup decimal not null,
	unit_name varchar(50) not null,
	--constraints
	constraint non_neg_price check(b_price >= 0),
	constraint non_neg_markup check(markup >= 0)
);

create table companies (
	id integer primary key,
	"name" varchar(100) not null,
	description varchar(200),
	workers_number integer,
	--constraints
	constraint non_neg_workers check((workers_number is null) or (workers_number >= 0))
);

create table sup_prod_cons (
	supply_id integer not null,
	product_id integer not null,
	volume integer not null,
	primary key (supply_id, product_id),
	--constraints
	constraint non_neg_volume check(volume >= 0),
	constraint supply_fkey foreign key (supply_id) references supplies(id) on delete cascade,
	constraint products_fkey foreign key (product_id) references products(id)
);
alter table supplies add constraint company_fkey foreign key (company_id) references companies(id);