---READ UNCOMMITED CHECK
--Дублируем
create table dup_spc as (select * from sup_prod_cons spc);
--T1
begin transaction isolation level read uncommitted;
	update sup_prod_cons
	set volume = volume * 2;
	commit;
end;
--T2
begin transaction isolation level read uncommitted;
	select spc.volume as newer, ds.volume as older, spc.volume / ds.volume as coef 
	from sup_prod_cons spc 
	join dup_spc ds on spc.supply_id = ds.supply_id and spc.product_id = ds.product_id
	where spc.volume / ds.volume = 2;
	update sup_prod_cons
	set volume = volume * 3;
	commit;
end;
--ПРОВЕРКА
select spc.volume as newer, ds.volume as older, spc.volume / ds.volume as coef
from sup_prod_cons spc 
join dup_spc ds on spc.supply_id = ds.supply_id and spc.product_id = ds.product_id
where spc.volume / ds.volume <> 6;
--ЧИСТИМ
update sup_prod_cons as spc
set volume = ds.volume 
from dup_spc as ds
where spc.supply_id = ds.supply_id and spc.product_id = ds.product_id;
drop table dup_spc;
---READ UNCOMMITTED CHECK END

---READ COMMITTED CHECK
--Дублируем
create table dup_spc as (select * from sup_prod_cons spc);
--T1
begin transaction isolation level read committed;
	update sup_prod_cons
	set volume = volume * 2;
	commit;
end;
--T2
begin transaction isolation level read committed;
	select *
	from sup_prod_cons spc 
	where spc.supply_id between 20 and 25;
	select *
	from sup_prod_cons spc 
	where spc.supply_id between 20 and 25;
	commit;
end;
--ЧИСТИМ
update sup_prod_cons as spc
set volume = ds.volume 
from dup_spc as ds
where spc.supply_id = ds.supply_id and spc.product_id = ds.product_id;
drop table dup_spc;
---READ COMMITTED CHECK END

---REPEATABLE READ CHECK
--Дублируем
create table dup_spc as (select * from sup_prod_cons spc);
--T1
begin transaction isolation level repeatable read;
	insert into sup_prod_cons values (40, 3, 1);
	commit;
end;
--T2
begin transaction isolation level repeatable read;
	select * from sup_prod_cons where volume < 2;
	select * from sup_prod_cons where volume < 2;
	commit;
end;
--ЧИСТИМ
delete from sup_prod_cons where supply_id = 40 and product_id = 3;
delete from sup_prod_cons where supply_id = 40 and product_id = 4;
update sup_prod_cons as spc
set volume = ds.volume 
from dup_spc as ds
where spc.supply_id = ds.supply_id and spc.product_id = ds.product_id;
drop table dup_spc;
---REPEATABLE READ CHECK END

---SERIALIZABLE CHECK
--Дублируем
create table dup_spc as (select * from sup_prod_cons spc);
--T1
begin transaction isolation level serializable;
	insert into sup_prod_cons values (40, 3, 1);
	commit;
end;
--T2
begin transaction isolation level serializable;
	select * from sup_prod_cons where volume < 2;
	select * from sup_prod_cons where volume < 2;
	commit;
end;
--ЧИСТИМ
delete from sup_prod_cons where supply_id = 40 and product_id = 3;
delete from sup_prod_cons where supply_id = 40 and product_id = 4;
update sup_prod_cons as spc
set volume = ds.volume 
from dup_spc as ds
where spc.supply_id = ds.supply_id and spc.product_id = ds.product_id;
drop table dup_spc;
---SERIALIZABLE CHECK END