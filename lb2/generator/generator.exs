companies = [
  {"Makima LLC", "Лесозаготовки", "100"},
  {"ООО Гражданская Оборона", "Пекарня", "200"},
  {"СибНефтеТрансМемГаз", "Добыча полезных ископаемых", "39"},
  {"Пакостин INC", "Автопром", "NULL"},
  {"Samsung", "Электротехника", "10000"},
  {"Polaroid", "Фототехника", "NULL"},
  {"Razer", "NULL", "234"},
  {"Газпром", "Добыча полезных ископаемых", "32000"},
  {"Telegram LLC", "Телекоммуникации", "12432"},
  {"BOSH", "NULL", "NULL"}
]

if length(companies) < 10, do: exit("Too short companies len: #{length(companies)}")

IO.puts("---КОМПАНИИ")

for {{n, p, s}, i} <- Enum.with_index(companies),
    do: IO.puts("INSERT INTO companies VALUES (#{i}, '#{n}', '#{p}', #{s});")

IO.puts("---\n")

products = [
  {"Вода", "л"},
  {"Цемент", "м3"},
  {"Сухари", "кг"},
  {"Моторы", "шт"},
  {"Станки фрезеровочные", "шт"},
  {"Нефть неочищенная", "м3"},
  {"Процессоры", "шт"},
  {"Шапки", "шт"},
  {"Фрезеровочные станки", "шт"},
  {"Лимонад", "л"}
]

products =
  Enum.map(products, fn {n, un} ->
    {n, Float.round(:rand.uniform(1000) + :rand.uniform(), 2),
     Float.round(:rand.uniform(400) + :rand.uniform(), 2), un}
  end)

if length(companies) < 10, do: exit("Too short companies len: #{length(companies)}")

IO.puts("---ТОВАРЫ")

for {{n, p, m, u}, i} <- Enum.with_index(products),
    do: IO.puts("INSERT INTO products VALUES (#{i}, '#{n}', #{p}, #{m}, '#{u}');")

IO.puts("---\n")

IO.puts("---ПОСТАВКИ")

for ind <- 1..500 do
  prods = Enum.take_random(Enum.with_index(products), :rand.uniform(5))
  [{_, comp_id}] = Enum.take_random(Enum.with_index(companies), 1)
  date = "202#{:rand.uniform(5)}-#{:rand.uniform(12)}-#{:rand.uniform(28)}"
  IO.puts("INSERT INTO supplies VALUES (#{ind}, 0, '#{date}', #{comp_id});")

  for {_, pid} <- prods do
    vol = :rand.uniform(100)
    IO.puts("INSERT INTO sup_prod_cons VALUES (#{ind}, #{pid}, #{vol});")
  end
end

IO.puts("---\n")
