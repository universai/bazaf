with glav as (
	select ct."НОМЕР_СТ", min(ct."ФИО") as "ФИО", array_agg(rab."НОМЕР_РБ") as main_rabs
	from РАБОТЫ rab
	join СОТРУДНИКИ ct on rab.ГЛАВНЫЙ = ct."НОМЕР_СТ" 
	group by ct."НОМЕР_СТ" 
)
select glav.НОМЕР_СТ, min(glav."ФИО") as ФИО
from glav
join СПИСКИ_СТ cc on cc."СОТР" = glav.НОМЕР_СТ
where not cc.РАБОТА = any(glav.main_rabs)
group by glav.НОМЕР_СТ;