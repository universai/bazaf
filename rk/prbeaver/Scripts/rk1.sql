select ОТДЕЛ, min(НАЗВ_ОТ) НАЗВАНИЕ, count(*) as СОТРУДНИКОВ
from СОТРУДНИКИ ст, ОТДЕЛЫ от
where ст.ОТДЕЛ = от.НОМЕР_ОТ 
group by ОТДЕЛ
order by 3 desc;

--ЮРА
select rab."НАЗВАНИЕ", count(distinct st."ОТДЕЛ") 
from ОТДЕЛЫ ot, РАБОТЫ rab, СПИСКИ_СТ sst, СОТРУДНИКИ st
where  sst.СОТР = st.НОМЕР_СТ and rab.НОМЕР_РБ = sst.РАБОТА and ot.НОМЕР_ОТ = st.ОТДЕЛ
group by rab."НАЗВАНИЕ";