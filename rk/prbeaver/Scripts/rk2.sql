--NUMBER 1
select ct."НОМЕР_СТ", min(ct."ФИО") as ФИО
from СОТРУДНИКИ as ct
where ct.НОМЕР_СТ in (select distinct РУКОВОД from СОТРУДНИКИ)
group by ct."НОМЕР_СТ"
except
select ct."НОМЕР_СТ", min(ct."ФИО") 
from СОТРУДНИКИ as ct
where ct.НОМЕР_СТ in (select distinct НАЧАЛЬНИК from ОТДЕЛЫ)
group by ct."НОМЕР_СТ";

--NUMBER 2
select ct.РУКОВОД as НОМЕР_СТ, min(ruk."ФИО") as ФИО, count(*) as ПОДЧИН
from СОТРУДНИКИ ct
join СОТРУДНИКИ ruk on ct."РУКОВОД" = ruk."НОМЕР_СТ" 
--where ct.РУКОВОД notnull
group by ct."РУКОВОД"
having count(*) >= 2;