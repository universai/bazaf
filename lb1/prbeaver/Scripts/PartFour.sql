with full_tickets as (select pinf.passenger_id, pinf.passenger_name, pinf.ticket_no, con.flight_id 
from tickets as pinf, ticket_flights as con
where pinf.ticket_no = con.ticket_no)
select 
	ft.passenger_id,
	min(ft.passenger_name) as passenger_name, 
	sum(fls.scheduled_arrival - fls.scheduled_departure) as sum_flight_dur
from full_tickets as ft, flights as fls
where ft.flight_id = fls.flight_id
group by ft.passenger_id
order by sum_flight_dur DESC;