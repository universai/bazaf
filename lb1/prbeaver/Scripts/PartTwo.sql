SELECT airport_code, timezone, count(*) OVER(partition by timezone) as same_time
from airports_data as ad
order by airport_code;