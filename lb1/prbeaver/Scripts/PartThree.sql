with latencies as (
select 
	flight_id, 
	(actual_departure - scheduled_departure) as dep_latency,
	(actual_arrival  - scheduled_arrival) as arr_latency
from flights
where not actual_arrival is null)
select 
	flight_id, 
	(arr_latency - dep_latency) as extra_time
from latencies
order by extra_time DESC;
