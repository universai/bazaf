select amount, sale, amount - sale as costs from (
select amount, round((amount / 100), 2) * 30 as sale
from bookings.ticket_flights limit 100
) as middle;