companies = 1_000_000
products = 1_000_000
pdescrs = products
units = 6
sups = 1_000_000
supprodcons = 100_000_000

# additional spc
IO.puts("Preparing")

combs = (DBGen.Accuracy.comb(1, sups) * DBGen.Accuracy.comb(1, products)) |> round

p = 1 / combs
add = DBGen.Accuracy.range_sum(0, 100_000_000, p) |> ceil()

IO.puts("Estimated repeating rows: #{add}")
supprodcons = supprodcons + add
IO.puts("Preparations done")
#

{:ok, f} = File.open("sql/Copy.sql", [:write, :utf8])
IO.puts(f, "\\copy units from ./res/man/units.csv with (format csv);")
File.close(f)

compgen = fn unq_id ->
  descr = fn ->
    Enum.reduce(0..:rand.uniform(20), [], fn _, acc -> [Rword.get() | acc] end)
    |> Enum.join(" ")
    |> String.slice(0, 200)
  end

  [
    unq_id,
    "#{Rword.get(:subj)} #{Rword.get(:noun)}",
    descr.(),
    :rand.uniform(100_000)
  ]
  |> Enum.join(",")
end

DBGen.make(compgen, "companies", companies)

prodgen = fn uniq_id ->
  subj = Rword.get(:subj, :all)
  noun = Rword.get(:noun, :all)
  [markup, price] = [:rand.uniform(10_000), :rand.uniform(5_000)] |> Enum.sort()

  [
    uniq_id,
    "#{subj[:ru]} #{noun[:ru]}",
    ~s("{""en"": ""#{subj[:en]} #{noun[:en]}""}"),
    price,
    markup,
    :rand.uniform(units)
  ]
  |> Enum.join(",")
end

DBGen.make(prodgen, "products", products)

pdescrgen = fn uniq_id ->
  descr = fn ->
    Enum.reduce(0..:rand.uniform(100), [en: [], ru: []], fn _, acc ->
      word = Rword.get(:rand, :all)
      [en: [word[:en] | acc[:en]], ru: [word[:ru] | acc[:ru]]]
    end)
    |> then(fn en ->
      [
        en[:ru] |> Enum.join(" ") |> String.slice(0, 1000),
        en[:en] |> Enum.join(" ") |> String.slice(0, 1000)
      ]
    end)
    |> then(fn [r, e] ->
      "\"{#{r},#{e}}\""
    end)
  end

  [
    uniq_id,
    descr.()
  ]
  |> Enum.join(",")
end

DBGen.make(pdescrgen, "p_descriptions", pdescrs)

supgen = fn uniq_id ->
  [
    uniq_id,
    # SHOULD BE FIXED IN SQL
    0,
    "\"20#{:rand.uniform(3) - 1}#{:rand.uniform(9)}-#{:rand.uniform(12)}-#{:rand.uniform(28)}\"",
    :rand.uniform(companies)
  ]
  |> Enum.join(",")
end

DBGen.make(supgen, "supplies", sups)

supprodcgen = fn _ ->
  [
    :rand.uniform(sups),
    :rand.uniform(products),
    :rand.uniform(10_000)
  ]
  |> Enum.join(",")
end

DBGen.make(supprodcgen, "sup_prod_cons", supprodcons)
