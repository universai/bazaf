\copy units from ./res/man/units.csv with (format csv);
\copy companies from ./res/companies.csv with (format csv);
\copy products from ./res/products.csv with (format csv);
\copy p_descriptions from ./res/p_descriptions.csv with (format csv);
\copy supplies from ./res/supplies.csv with (format csv);
\copy sup_prod_cons from ./res/sup_prod_cons.csv with (format csv);
