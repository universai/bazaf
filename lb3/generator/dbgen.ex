defmodule DBGen do
  @chunksize 2000
  @pullsize 16

  @doc """
  Generate db csv
  """
  @spec make((integer() -> binary()), binary(), integer()) :: :ok
  def make(valuegen, filename, amount, start_from \\ 1) do
    IO.puts("Working on #{filename}")
    {:ok, csv_f} = File.open("res/#{filename}.csv", [:write, :utf8])
    wrt_pid = spawn(fn -> writer(csv_f, ceil(amount / @chunksize)) end)

    tstart = :os.system_time(:millisecond)
    motherproc(valuegen, wrt_pid, amount, start_from)
    tend = :os.system_time(:millisecond)

    send(wrt_pid, {self(), {:done, tend - tstart}})

    receive do
      :free ->
        File.close(csv_f)
        {:ok, f} = File.open("sql/Copy.sql", [:append, :utf8])
        IO.puts(f, "\\copy #{filename} from ./res/#{filename}.csv with (format csv);")
        File.close(f)
    end
  end

  @spec motherproc((integer() -> binary()), pid(), integer(), integer()) :: :ok
  defp motherproc(valuegen, wrt_pid, amount, current \\ 1, alive \\ 0) do
    if alive < @pullsize && current < amount do
      ncurrent = current + @chunksize - 1
      m_pid = self()
      spawn(fn -> chunk(valuegen, current..ncurrent, wrt_pid, m_pid) end)
      motherproc(valuegen, wrt_pid, amount, ncurrent + 1, alive + 1)
    else
      receive do
        {:done, :ok} ->
          motherproc(valuegen, wrt_pid, amount, current, alive - 1)
      after
        2_000 -> :ok
      end
    end
  end

  @spec chunk((integer() -> binary()), Range.t(), pid(), pid()) :: :ok
  defp chunk(valuegen, range, wrt_pid, m_pid) do
    send(
      wrt_pid,
      {m_pid, {:insert, Enum.map(range, fn ind -> valuegen.(ind) end) |> Enum.join("\n")}}
    )
  end

  @spec writer(File.io_device(), integer(), integer()) :: :ok
  defp writer(file, amount, inserted \\ 0) do
    receive do
      {m_pid, {:insert, value}} ->
        ins = inserted + 1
        IO.puts(file, value)

        IO.write(
          "\rInserted #{ins} out of #{amount} chunks; Writer message queue: #{Process.info(self(), :message_queue_len) |> elem(1)} mes\t"
        )

        send(m_pid, {:done, :ok})
        writer(file, amount, ins)

      {b_pid, {:done, time}} ->
        IO.puts("\nProcess is done in #{(time / 1000) |> round} sec")
        send(b_pid, :free)
        :ok
    end
  end
end

defmodule DBGen.Accuracy do
  defp fact(n, to \\ 0)
  defp fact(n, to) when n == to and to >= 0, do: 1
  defp fact(n, to) when n > to and to >= 0, do: n * fact(n - 1, to)

  @doc """
  Get combinations
  """
  @spec comb(integer(), integer()) :: float()
  def comb(k, n) when n >= k do
    [mn, mx] = Enum.sort([k, n - k])
    fact(n, mx) / fact(mn)
  end

  @doc """
  Get range sum
  """
  @spec range_sum(integer(), integer(), float()) :: float()
  def range_sum(from, to, rate \\ 1)
  def range_sum(from, to, rate) when from == to, do: to * rate
  def range_sum(from, to, rate), do: from * rate + range_sum(from + 1, to, rate)
end

defmodule Rword do
  @nouns (fn ->
            Path.wildcard("./static/nouns*.txt")
            |> Enum.map(
              &(Regex.run(~r/\w+_(\w+).txt/, &1)
                |> then(fn [name, lang] ->
                  {:ok, f} = File.open("./static/#{name}", [:read, :utf8])
                  words = IO.read(f, :eof) |> String.trim_trailing("\n") |> String.split("\n")
                  File.close(f)
                  {String.to_atom(lang), words}
                end))
            )
            |> then(fn kword ->
              keys = Keyword.keys(kword)

              Enum.zip_with(Keyword.values(kword), &Enum.zip(keys, &1))
            end)
          end).()

  @subjs (fn ->
            Path.wildcard("./static/subjs*.txt")
            |> Enum.map(
              &(Regex.run(~r/\w+_(\w+).txt/, &1)
                |> then(fn [name, lang] ->
                  {:ok, f} = File.open("./static/#{name}", [:read, :utf8])
                  words = IO.read(f, :eof) |> String.trim_trailing("\n") |> String.split("\n")
                  File.close(f)
                  {String.to_atom(lang), words}
                end))
            )
            |> then(fn kword ->
              keys = Keyword.keys(kword)

              Enum.zip_with(Keyword.values(kword), &Enum.zip(keys, &1))
            end)
          end).()

  @doc """
  Get random noun or subject
  """
  @spec get(:noun | :subj | :rand, :ru | :en | :all) :: binary()
  def get(what \\ :rand, lang \\ :ru)

  def get(:noun, lang),
    do:
      Enum.random(@nouns)
      |> then(&unless lang == :all, do: Keyword.fetch(&1, lang) |> elem(1), else: &1)

  def get(:subj, lang),
    do:
      Enum.random(@subjs)
      |> then(&unless lang == :all, do: Keyword.fetch(&1, lang) |> elem(1), else: &1)

  def get(:rand, lang) do
    case :rand.uniform() do
      x when x < 0.5 -> get(:noun, lang)
      _ -> get(:subj, lang)
    end
  end
end
