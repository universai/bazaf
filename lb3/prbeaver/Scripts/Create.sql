create table companies (
	id serial,
	"name" varchar(150),
	descr varchar(200),
	workers_number integer
);

create table supplies (
	id serial,
	"cost" decimal,
	"date" date,
	company_id integer
);

create table units (
	id serial,
	"name" varchar(50)
);

create table products (
	id serial,
	"name" varchar(150),
	intern_names jsonb,
	buying_price decimal,
	markup decimal,
	unit_id integer
);

create table p_descriptions (
	prod_id integer,
	descr text[]
);

create table sup_prod_cons (
	sup_id integer,
	prod_id integer,
	volume integer
);