alter table companies
	add primary key (id),
	alter column "name" set not null,
	add constraint non_neg_workers_number check ((workers_number is null) or (workers_number >= 0));

alter table supplies
	add primary key (id),
	alter column "cost" set not null,
	alter column "date" set not null,
	alter column company_id set not null,
	add constraint company_fkey foreign key (company_id) references companies(id),
	add constraint non_neg_cost check ("cost" >= 0);

alter table units
	add primary key (id),
	alter column "name" set not null,
	add unique ("name");

alter table products
	add primary key (id),
	alter column "name" set not null,
	alter column buying_price set not null,
	alter column markup set not null,
	alter column unit_id set not null,
	add constraint unit_fkey foreign key (unit_id) references units(id),
	add constraint non_neg_buying_price check (buying_price >= 0),
	add constraint non_neg_markup check (markup >= 0);

alter table p_descriptions
	add primary key (prod_id),
	alter column descr set not null,
	add constraint prod_fkey foreign key (prod_id) references products(id) on delete cascade,
	add constraint existing_descr check (array_length(descr, 1) is not null);

alter table sup_prod_cons
	add primary key (sup_id, prod_id),
	alter column sup_id set not null,
	alter column prod_id set not null,
	alter column volume set not null,
	add constraint sup_fkey foreign key (sup_id) references supplies(id) on delete cascade,
	add constraint prod_fkey foreign key (prod_id) references products(id),
	add constraint positive_volume check (volume > 0);