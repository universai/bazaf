create view sup_revenues as
select s.id as id, sum(spc.volume * p.markup) as revenue, min(s."date") as "date", min(s.company_id) as company_id
from supplies s 
join sup_prod_cons spc on s.id = spc.sup_id 
join products p on spc.prod_id = p.id
group by s.id;

drop view sup_revenues;