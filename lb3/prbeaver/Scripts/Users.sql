create user test;

grant select, insert, update on products to test;
grant select, update on companies to test;
grant select on supplies to test;


create role revenuer;
grant select on sup_revenues to revenuer;
grant revenuer to test;