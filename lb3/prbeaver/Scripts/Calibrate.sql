---MERGE REPEATING PRODS
with rep_spc as (
	select sup_id, prod_id, sum(volume) as volume
	from sup_prod_cons spc
	group by sup_id, prod_id
	having count(*) > 1
), del as (
	delete from sup_prod_cons spc
	using rep_spc rs
	where spc.sup_id = rs.sup_id and spc.prod_id = rs.prod_id
)
insert into sup_prod_cons 
select * from rep_spc;

---UPDATE COST
with swcost as (
	select s.id as id, sum(spc.volume * (p.buying_price + p.markup)) as "cost"
	from supplies s 
	join sup_prod_cons spc on s.id = spc.sup_id 
	join products p on spc.prod_id = p.id 
	group by s.id
)
update supplies s
set "cost" = sw."cost"
from swcost sw
where s.id = sw.id;

