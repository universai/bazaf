select *
from sup_revenues sr;
with spls as (
select s.*, array_agg((p.id, spc.volume)) as volumes, jsonb_agg(p.*) as products
from supplies s
join sup_prod_cons spc on spc.sup_id = s.id
join products p on spc.prod_id = p.id
where s.date = '2023-12-04'
group by s.id
)
select sp.id, sp.cost, sp.date, to_json(c) as company, sp.volumes, sp.products
from spls sp
join companies c on sp.company_id = c.id
order by sp.id;