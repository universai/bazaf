select * from products p;

insert into products values (1222333, 'Пепси', '{ "en": "Pepsi", "by": "Пэпси"}', 2000, 1000, 2);
delete from products where id = 1222333;

explain analyze
update products 
set intern_names = intern_names || '{ "gr" : "Pps" }'
where id = 1222333;

explain analyze
update supplies as s
set "cost" = "cost"
from sup_prod_cons as spc
where s.id = spc.sup_id and 1222333 = spc.prod_id;

create index on sup_prod_cons(sup_id);
create index on sup_prod_cons(prod_id);
drop index sup_prod_cons_sup_id_idx;
drop index sup_prod_cons_prod_id_idx;

explain analyze
select *
from supplies s
join sup_prod_cons spc on s.id = spc.sup_id
where extract (year from s.date) = 2023;

create index on supplies (extract (year from "date"));
drop index supplies_extract_idx;

explain analyze
select * 
from products p
where "name" LIKE 'решительный %';

explain analyze
select * 
from products p
where intern_names->>'en' = 'outstanding muse';

create index on products ((intern_names->>'en'));
drop index products_expr_idx;

explain analyze
select * 
from products p
where intern_names @> '{ "en" : "outstanding muse" }';

create index on products using gin (intern_names);
drop index products_intern_names_idx;

explain analyze
select *
from p_descriptions pd
where to_tsvector('english', descr[2]) @@ to_tsquery('cable & hood');

explain analyze
select *
from p_descriptions pd
where to_tsvector('english', product_get_descr(prod_id, 'en')) @@ 'cable & hood';

create index on p_descriptions using gin (to_tsvector('english', product_get_descr(prod_id, 'en')));
drop index p_descriptions_to_tsvector_idx;

