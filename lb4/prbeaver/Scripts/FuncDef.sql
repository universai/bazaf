create or replace function pn_match_pd()
returns trigger as
$$
declare 
len integer := 0;
newdescr text[] := null;
oldescr text[] := null;
curiter cursor for select ol.lan, ol.ind + 1 as olind, nw.ind + 1 as newind
		from (
			select lan.nm as lan, lan.ind as ind 
			from jsonb_object_keys(old.intern_names) 
			with ordinality lan(nm, ind)
		) as ol
		join (
			select lan.nm as lan, lan.ind as ind 
			from jsonb_object_keys(new.intern_names) 
			with ordinality lan(nm, ind)
		) as nw on ol.lan = nw.lan;
iter record;
begin 
	if tg_name = 'inserted_prod' then
	len = (select count(*) from (select jsonb_object_keys(new.intern_names)) ks) + 1;
	insert into p_descriptions values (new.id, array_fill(null::text, array[len]));
	end if;
	if tg_name = 'updated_prod_pnpd' then
	len = (select count(*) from jsonb_object_keys(new.intern_names));
	newdescr = array_fill(null::text, array[len]);
	oldescr = (select descr from p_descriptions where prod_id = old.id);
	newdescr[1] = oldescr[1];
	for iter in curiter loop
		newdescr[iter.newind] = oldescr[iter.olind];
	end loop;
	update p_descriptions
	set descr = newdescr
	where prod_id = new.id;
	end if;
	return new;
end;
$$ language 'plpgsql';

create trigger inserted_prod
	after insert on products
	for each row
	execute function pn_match_pd();

create trigger updated_prod_pnpd
	after update on products
	for each row
	execute function pn_match_pd();

create or replace function product_set_descr(pid integer, dscr text, lang varchar(10) default null)
returns void as
$$
declare 
nth integer := 0;
begin 
	if lang is null then
	update p_descriptions pd
	set descr[1] = dscr
	where pd.prod_id = pid;
	else
	nth = (select ind from (
		select lan.nm as nm, lan.ind + 1 as ind from jsonb_object_keys((
			select intern_names from products p where p.id = pid)
		) with ordinality lan(nm, ind)
	) as tbl where tbl.nm = lang);
	if nth is null then
		raise exception 'Language "%" is not available for this product', lang;
	else
		update p_descriptions pd
		set descr[nth] = dscr
		where pd.prod_id = pid;
	end if;
	end if;
end;
$$ language 'plpgsql';

create or replace function product_get_descr(pid integer, lang varchar(10) default null, erraise bool default false)
returns text as
$$
declare 
nth integer := 0;
begin 
	if lang is null then
	return (select descr[1] from p_descriptions pd where pd.prod_id = pid);
	else
	nth = (select ind from (
		select lan.nm as nm, lan.ind + 1 as ind from jsonb_object_keys((
			select intern_names from products p where p.id = pid)
		) with ordinality lan(nm, ind)
	) as tbl where tbl.nm = lang);
	if nth is null then
		if erraise then
			raise exception 'Language "%" is not available for this product', lang;
		else
			return null;
		end if;
	else
		return (select descr[nth] from p_descriptions pd where pd.prod_id = pid);
	end if;
	end if;
end;
$$ language 'plpgsql' IMMUTABLE;